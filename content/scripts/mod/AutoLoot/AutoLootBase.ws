/*
* Base file of the mod
* Feel free to edit/redistribute.
*/
class AutoLootBase extends AutoLootFilters
{
	// constructor
	public final function InitBase()
	{
		super.InitFilters();
	}
	
	// this function tries to loot as many items as possible (if the filters allow it) and returns a boolean value, whether the container still has items or not
	public function LootContainer( container : W3Container ) : bool
	{
		var ContainerInventory : CInventoryComponent;
		var StillHasItems : bool;
		var ContainerAllItems : array<SItemUniqueId>;
		var ContainerLength : int;
		var i : int;
		var NotificationText : string;
		var NotificationShouldDisplay : bool;
		var TreasureHuntContainer : W3treasureHuntContainer;
		var ShouldPlaySound : bool;
		
		ShouldPlaySound = IsSoundEnabled();
		
		// fix for treasure containers
		if( (W3treasureHuntContainer)container )
		{
			TreasureHuntContainer = (W3treasureHuntContainer)container;
			TreasureHuntContainer.ProcessOnLootedEvents();
		}

		// we begin with the assumption we will not display a notification
		NotificationShouldDisplay = false;
		
		// we initialize the notification text
		NotificationText = "Loot:";
		
		// we get the container's inventory in an array
		ContainerInventory = container.GetInventory();
		
		// we check if the container isn't already empty
		if (ContainerInventory.GetAllItemsQuantity() > 0)
		{
			StillHasItems = true;
		}
		else
		{
			return false;
		}
		
		// first we fill an array with all the items
		ContainerInventory.GetAllItems( ContainerAllItems );
		
		// we cleanse our list from technical items, that aren't supposed to be seen by the player.
		for( i = ContainerAllItems.Size()-1; i >= 0; i -= 1)
			if( ContainerInventory.ItemHasTag(ContainerAllItems[i], theGame.params.TAG_DONT_SHOW ) && !ContainerInventory.ItemHasTag(ContainerAllItems[i], 'Lootable' ) )
				ContainerAllItems.Erase(i);
		
		// we get the number of items in the container, in order to parse them
		ContainerLength = ContainerAllItems.Size();
		
		// we go through each item and try to loot them if they match the filters
		for( i = 0; i < ContainerLength; i+=1 )
		{
			// Crash fix: disable autoloot on oils and potions that are refillable!
			if( ContainerInventory.ItemHasTag(ContainerAllItems[i], 'QuickSlot') )
				break;
			
			//--- --- Customize filters here! --- ---
			if(
				// Uncomment what you want!
				// You can combine different conditions here!
				/* Important info on && (AND) and on || (OR)
				* Use && before filters to only AutoLoot items with ALL the filters active.
				* Use || before filters to AutoLoot items that have at least one of those filters active
				*
				*  Examples:
				*		Herbs OR armor: 
				*			uncomment Herb(...) || Armor(...) ...
				*
				*		No accidental stealing AND only Armor that is more expensive than 100 coins: 
				*			uncomment  NoAccidentalStealing(...) && PriceFilter(...) && Armor(...)
				*/
				
				NoAccidentalStealing( container )
				//&& NumberOfItemsInContainerFilter( length )
			    //&& WeightFilter( container, ContainerAllItems[i] )
				//&& PriceFilter( container, ContainerAllItems[i] )
				
				//&& Herb( container )
				//&& Armor( container, ContainerAllItems[i] )
				//&& Weapon( container, ContainerAllItems[i] )
				//&& Foodstuff( container, ContainerAllItems[i] )
				//&& ItemQuality( container, ContainerAllItems[i] ) 
				
			)
			//--- --- Do not modify anything below here! --- ---
			{
				// we also add the item to the notification
				NotificationText = NotificationText + GetNotificationItem( container, ContainerAllItems[i] );	
				
			    // if an item matches the filters it's looted using the LootItem function
				LootItem( container, ContainerAllItems[i] );
				
				// We remember to display a notification because we looted at least one item
				NotificationShouldDisplay = true;
			}
			
		}
		
		// Cleanup after looting
		ContainerInventory.GetAllItems( ContainerAllItems );
		for( i = ContainerAllItems.Size() - 1; i >= 0; i -= 1 )
		{
			if( (ContainerInventory.ItemHasTag(ContainerAllItems[i],theGame.params.TAG_DONT_SHOW) || ContainerInventory.ItemHasTag(ContainerAllItems[i],'NoDrop') ) && !ContainerInventory.ItemHasTag(ContainerAllItems[i], 'Lootable' ) )
			{
				ContainerAllItems.Erase(i);
			}
		}
		
		// if we looted anything we display notification
		if( NotificationShouldDisplay == true && GetNotificationTime() > 0.0 )	
			DisplayNotification( NotificationText, GetNotificationTime() );
		
		// we check if the containers still has items that the user may need
		if (ContainerInventory.GetAllItemsQuantity() > 0)
			StillHasItems = true;
		else
			StillHasItems = false;
		
		// if we looted anything we play the sound
		if( (ShouldPlaySound == true) && NotificationShouldDisplay == true )
			theSound.SoundEvent("gui_loot_popup_close");
		
		if(StillHasItems == false)
		{
			container.AutoLootCleanup();
		}
		
		return StillHasItems;
	}
	
	// loots items, sound-less
	private function LootItem( Container : W3Container, Item : SItemUniqueId ) : void 
	{
		var ItemQuantity : int;
		
		ItemQuantity = Container.GetInventory().GetItemQuantity( Item );
		
		if( Container.GetInventory().ItemHasTag(Item, 'Lootable' ) || !Container.GetInventory().ItemHasTag(Item, 'NoDrop') && !Container.GetInventory().ItemHasTag(Item, theGame.params.TAG_DONT_SHOW))
		{
			Container.GetInventory().NotifyItemLooted( Item );
			Container.GetInventory().GiveItemTo( GetWitcherPlayer().inv, Item, ItemQuantity, true, false, true );
		}
		
		if( Container )
		{
			Container.InformClueStash();
		}
	}
	
	// this function gets the name of the item, localized and colored (optional)!
	private function GetNotificationItem( container : W3Container, Item : SItemUniqueId ) : string
	{
		var ReturnedItemName : string; // temp value to fill
		var ItemName : string;
		var AutoLootItemRarity : int;
		var AutoLootItemColor : string;
		
		// we get the localized item's name
		ItemName = container.GetInventory().GetItemLocalizedNameByUniqueID( Item );
			
		ItemName = GetLocStringByKeyExt(ItemName);
		if ( ItemName == "" )
			ItemName = " ";
		
		// Thanks to Nexus user mcguffin for submiting code that allows multi-colored item names!
		if( IsNotificationColorEnabled() )
		{
			AutoLootItemRarity = container.GetInventory().GetItemQuality( Item );
			
			// Modify the tooltip colors! You need to use HTML color codes!
			if( AutoLootItemRarity == 2) AutoLootItemColor = "#0000FF"; // Master items
			else if (AutoLootItemRarity == 3) AutoLootItemColor = "#CCCC00"; // Magic items
			else if (AutoLootItemRarity == 4) AutoLootItemColor = "#844211"; // Relic items
			else if (AutoLootItemRarity == 5) AutoLootItemColor = "#006600"; // Witcher set items
			else AutoLootItemColor = "#000000"; // Common item
			
			ReturnedItemName = "<br/><font color='"+ AutoLootItemColor + "'>" + ItemName + "</font>";
		}
		else
			ReturnedItemName = "<br/>" + ItemName; 

			
		// This part adds the quantity to each item
		// Thanks to Nexus user mcguffin to submiting this part too.
		if( IsNotificationQuantityEnabled() && container.GetInventory().GetItemQuantity( Item ) > 1 )
			ReturnedItemName =  ReturnedItemName + " x " + container.GetInventory().GetItemQuantity( Item );
		
		return ReturnedItemName;
	}
}