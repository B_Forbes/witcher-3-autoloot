/*
* Utility functions
* Feel free to edit/redistribute.
*/
// prevent instantiation by declaring abstract
abstract class AutoLootUtilities 
{
	private const var ModVersion : string;
	
	default ModVersion = "1.6.0";
	
	// constructor
	protected function InitUtil()
	{
		
	}

	protected function DisplayNotification(str : string, optional time : float)
	{
		theGame.GetGuiManager().ShowNotification(str, time);
	}
}