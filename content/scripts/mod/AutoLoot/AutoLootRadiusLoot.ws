/*
* Radius loot part of the mod.
* Feel free to edit/redistribute.
*/
class AutoLootRadiusLoot extends AutoLootUserConfig
{
	public function LootInRadius()
	{
		var EntitiesFound : array<CGameplayEntity>;
		var i : int;
		var MaxContainers : int;
		var NumberOfContainers : int;
		var TempStoreOption : bool;
		var ContainerNum : int;
		
		if( !IsRadiusLootEnabled() )
			return;
		
		if( thePlayer.IsInCombat())
			return;
		
		// find containers in a range
		FindGameplayEntitiesInRange( 
			EntitiesFound, // array to store values
			thePlayer, // center of search box
			GetRadiusLootMaxDistance(), // maximum distance
			300);// maximum objects
		
		// we initialize variables
		NumberOfContainers = EntitiesFound.Size();
		MaxContainers = GetRadiusLootMaxObjects();
		
		if(MaxContainers <= 0)
			MaxContainers = 1;
			
		ContainerNum = 0;
		
		// we now parse the containers
		for( i = 0; i < NumberOfContainers; i += 1)
		{
			// we ignore some containers
			if( (W3Container) EntitiesFound[i] )
			{
				if( !((W3Container) EntitiesFound[i]).IsEmpty() )
				{
					// we increase the containers found number
					if( ((W3Container) EntitiesFound[i]).TryToAutoLoot() == true )
					{
						ContainerNum += 1;
						((W3Container) EntitiesFound[i]).AutoLootCleanup();
					}
						
					// do not loot above the max number
					if(ContainerNum == MaxContainers)
							return;
				}
			}
		}
	}
}