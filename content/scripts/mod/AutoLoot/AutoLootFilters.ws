/*
* Filter functions
* Feel free to edit/redistribute.
*/
// prevent instantiation by declaring abstract
abstract class AutoLootFilters extends AutoLootUserConfig
{
	protected function InitFilters()
	{
		super.InitConfig();
	}

	// Filters section --- Customize filters here!
	function NoAccidentalStealing(container : W3Container ) : bool
	{
		// --- No Accidental Stealing Filter
		// AutoLoots EVERYTHING as long as
		// taking from the container is NOT considered stealing
		if( container.disableStealing || (W3Herb)container || (W3ActorRemains)container )
			return true;
		else
		{
			return false;
		}
	}
	
	 function NumberOfItemsInContainerFilter( length : int ) : bool
	{
		// --- Numbers of items in container filter:
		// checks if container has more or less or exactly X items.
		//  *replace == with < if you want to have more than X items.
		//  *replace == with > if you want less than X itemStats
		//  *replace the 1 with any other non-negative number!
		if( 1 == length )
		{
			return true;
		} 
		else
		{
			return false;
		}
	}

	 function WeightFilter( container : W3Container, ItemID : SItemUniqueId ) : bool
	{
		// --- Weight Filter:
		// checks if item has a certain weight
		//  replace the == with < or > sign to check if the item is lighter than / heavier than the weight.
		//  replace 0 with whatever weight you want
		//  0 is a real number: it could be 1.2, 222.561, 3.1415 etc.
		if( container.GetInventory().GetItemEncumbrance( ItemID ) == 0 )
		{
			return true;
		} 
		else
		{
			return false;
		}
	}

	 function PriceFilter( container : W3Container, ItemID : SItemUniqueId ) : bool
	{
		// --- Price Filter:
		// Checkss if item has a certain price
		//  Replace < with == to autoloot items of an exact price.
		//  Replace < with == to autoloot items that are less expensive than X
		//   Replace 100 with any integer number.
		if( 100 < container.GetInventory().GetItemPrice( ItemID ) )
		{
			return true;
		} 
		else
		{
			return false;
		}
	}

	 function Herb( container : W3Container ) : bool
	{
		// --- Herbs filter
		// Only autoloots herbs
		if( (W3Herb)container )
		{
			return true;
		} 
		else
		{
			return false;
		}
	}

	 function Armor( container : W3Container, ItemID : SItemUniqueId ) : bool
	{
		// --- Armor filter
		// Only autoloots armor
		if( container.GetInventory().IsItemAnyArmor( ItemID ) )
		{
			return true;
		} 
		else
		{
			return false;
		}
	}

	 function Weapon( container : W3Container, ItemID : SItemUniqueId ) : bool
	{
		// --- Weapon filter
		// Only autoloots weapons
		if( container.GetInventory().IsItemWeapon( ItemID ) )
		{
			return true;
		} 
		else
		{
			return false;
		}
	}
		
	 function Foodstuff( container : W3Container, ItemID : SItemUniqueId ) : bool
	{	
		// --- Food and drinks filter
		// Only autoloots edible things
		if( container.GetInventory().IsItemFood( ItemID ) )
		{
			return true;
		} 
		else
		{
			return false;
		}
	}
	
	function ItemQuality( container : W3Container, ItemID : SItemUniqueId  ) : bool
	{
		// check if an item has at least a certain quality level
		// this version checks if item is at least at quality 2 (not common)
		// Quality of items can be:
		// 1 = common
		// 2 = masterwork
		// 3 = magical
		// 4 = relic
		// 5 = Witcher Gear
		if( 2 <= container.GetInventory().GetItemQuality( ItemID )  )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}