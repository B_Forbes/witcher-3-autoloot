/*
* Main configuration file of the mod
* Feel free to edit/redistribute.
*/
class AutoLootUserConfig extends AutoLootUtilities
{
	// settings/constants
	private const var NotificationTime : float;
	private const var EnableNotificationColor : bool;
	private const var EnableNotificationQuantity : bool;
	private const var EnableOnKillLoot : bool;
	private const var EnableRadiusLoot : bool;
	private const var RadiusLootMaxDistance : float;
	private const var RadiusLootMaxContainersAtOnce : int;
	private const var LootSoundEnabled : bool;
	
	/**===== User settings, edit here! =====*/
	default NotificationTime = 5000.0;
	
	default EnableNotificationColor = true;
	default EnableNotificationQuantity = true;
	
	default EnableOnKillLoot = true;
	default EnableRadiusLoot = true;
	
	default RadiusLootMaxDistance = 15.0;
	default RadiusLootMaxContainersAtOnce = 5;
	
	default LootSoundEnabled = true;
	/** ** Do not edit below here! ** */
	
	
	// constructor
	protected function InitConfig()
	{
		// call Utilities ctor
		super.InitUtil();
	}
	
	// Getters
	public function GetNotificationTime() : float
	{
		return NotificationTime;
	}
	
	public function IsNotificationColorEnabled() : bool
	{
		return EnableNotificationColor;
	}
	
	public function IsNotificationQuantityEnabled() : bool
	{
		return EnableNotificationQuantity;
	}
	
	public function IsOnKillEnabled() : bool
	{
		return EnableOnKillLoot;
	}
	
	public function IsRadiusLootEnabled() : bool
	{
		return EnableRadiusLoot;
	}
	
	public function GetRadiusLootMaxDistance() : float
	{
		return RadiusLootMaxDistance;
	}
	
	public function GetRadiusLootMaxObjects() : int
	{
		return RadiusLootMaxContainersAtOnce;
	}
	
	public function IsModEnabled() : bool
	{
		return true;
	}
	
	public function IsSoundEnabled() : bool
	{
		return LootSoundEnabled;
	}
}